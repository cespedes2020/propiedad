package Cespedes.bl;



public class Propiedad {
    //atributos
    private String nombre;
    private int codigo;
    private int cuartos;
    private String provincia;
    private double montoMensualidad;

    /**
     * Asi es como se documentan los parametros:
     * @param codigo es de tipo int y representa el codigo de la propiedad
     * @param nombre es de tipo String y representa el nombre de la propiedad
     * @param cuartos es de tipo int y representa el numero de cuartos de la propiedad
     * @param provincia es de tipo String y representa el nombre de la provincia de la propiedad
     * @param mensualidad es de tipo double y representa la mensualidad de la propiedad
     * **/

    public Propiedad(String nombre, int codigo, int cuartos, String provincia, double mensualidad) {
        this.nombre = nombre;
        this.codigo = codigo;
        this.cuartos = cuartos;
        this.provincia = provincia;
        this.montoMensualidad = mensualidad;
    }

    public String getNombre() {return nombre;}

    public void setNombre(String nombre) {this.nombre = nombre;}

    public int getCodigo() {return codigo;}

    public void setCodigo(int codigo) {this.codigo = codigo;}

    public int getCuartos() {return cuartos;}

    public void setCuartos(int cuartos) {this.cuartos = cuartos;}

    public String getProvincia() {return provincia;}

    public void setProvincia(String provincia) {this.provincia = provincia;}

    public double getMontoMensualidad() {return montoMensualidad;}

    public void setMontoMensualidad(double montoMensualidad) {this.montoMensualidad = montoMensualidad;}

    @Override
    public String toString() {
        return "Cespedes.bl.Propiedad{" +"nombre='" + nombre + '\'' +", codigo=" + codigo +", cuartos="
                + cuartos +", provincia='" + provincia + '\'' +", mensualidad=" + montoMensualidad +'}';
    }
}

