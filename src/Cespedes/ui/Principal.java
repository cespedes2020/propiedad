package Cespedes.ui;
/**
 * @author Jose Ignacio
 * @version V1.0
 * @since 10-02-2021
 * Esta clase gestiona cada una de las propiedades del sistema
 * **/
import java.io.BufferedReader;
import java.io.InputStreamReader;
//el asterisco de abajo me importa todas las clases que estan dentro de ui
//aca importo la clase propiedad, ambas, tanto la ui como la bl se deben importar
import Cespedes.bl.Propiedad;


public class Principal {
    //atributos
    static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    static Propiedad[] listaPropiedades = new Propiedad[10];
    public static void main(String[] args) throws Exception{
        menu();
    }
    public static void menu() throws Exception{
    int opci;
    do {
        System.out.println("***Bienvenido al sistema Propiedades***");
        System.out.println("1. Registrar una propiedad");
        System.out.println("2. Lista de propiedades");
        System.out.println("3. Verificar una propiedad");
        System.out.println("4. Total de los alquileres");
        System.out.println("5. Salir");
        System.out.println("Digite una opcion : ");
        opci = Integer.parseInt(br.readLine());
        //move to the ProcesarOpcion method with opcion
        procesarOpcion(opci);

    }while(opci !=5);
        }
public static void procesarOpcion(int opci) throws Exception{
        switch (opci){
            case 1: 
                registraPropiedad();
                break;
            case 2: 
                imprimirPropiedades();
                break;
            case 3: 
                buscarPropiedad();
                break;
            case 4: 
                sumaAlquiler();
                break;
            case 5:
                System.out.println("Gracias por su visita");
                System.exit(0);
            default:
                System.out.println("Opcion invalida");
        }
}
public static void registraPropiedad() throws Exception {
    System.out.println("Ingrese el nombre de la propiedad");
    String nombre = br.readLine();
    System.out.println("Ingrese el codigo");
    int codigo = Integer.parseInt(br.readLine());
    System.out.println("Ingrese el numero de cuartos");
    int cuartos = Integer.parseInt(br.readLine());
    System.out.println("Ingrese la provincia");
    String provincia = br.readLine();
    System.out.println("Ingrese la mensualidad");
    int mensualidad = Integer.parseInt(br.readLine());
    //instancio el nuevo objeto
    Propiedad propiedad = new Propiedad(nombre, codigo, cuartos, provincia, mensualidad);
    for (int x = 0; x < listaPropiedades.length; x++) {
        if (listaPropiedades[x] == null) {
            listaPropiedades[x] = propiedad;
            //esto hace que detenga mi codigo
            x = listaPropiedades.length;
        }

    }
}
    static public void imprimirPropiedades(){
        for (int i = 0; i < listaPropiedades.length; i++){
            if (listaPropiedades[i] != null){
                System.out.println(listaPropiedades[i].toString());
            }
        }
    }

    static public void buscarPropiedad() throws Exception{
        //Declaracion de variables
        boolean fueEncontrado = false;
        //indiceEncontrado es lo mismo que usar fueEncontrado solo que de una forma distinta
//        int indiceEncontrado = -1;
        Propiedad propiedadEncontrada = null;

        //User input/Pido info al usuario
        System.out.println("digite el nombre de la propiedad");
        String nombre = br.readLine();

        //Recore la lista y busca la info
        for (int i = 0; i < listaPropiedades.length; i++) {
            if (listaPropiedades[i].getNombre() == nombre) {
                fueEncontrado = true;
                propiedadEncontrada = listaPropiedades[i];
//                indiceEncontrado = i;
            }
        }

        //Muestra resultado del usuario
        if (fueEncontrado){
            System.out.println(propiedadEncontrada.toString());
//            System.out.println(listaPropiedades[indiceEncontrado].toString());
        } else {
            System.out.println("La propiedad no fue encontrada");
        }
    }

    static public void sumaAlquiler(){
        double total = 0;

        for (int i = 0; i < listaPropiedades.length; i++) {
            System.out.println("La propiedad : " + listaPropiedades[i].getNombre() + " tiene una mensualidad de: " + listaPropiedades[i].getMontoMensualidad());
            total = total + listaPropiedades[i].getMontoMensualidad();
        }

        System.out.println("La suma de todas las mensualidades seria: " + total);
    }
}


